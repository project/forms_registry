<?php
/**
 * @file
 * Config data for the Ajax Forms Test module
 */

/**
 * Provides config data for the Ajax Forms Test module
 */
class FormsAjaxTestConfig {
  //menu path
  const TEST_PAGE_PATH = 'test/ajax-forms';
  //form ID
  const AJAX_SELECT_EXAMPLE = 'forms_ajax_test_dependent_select_form';
}