<?php

/*
 * @file
 * Configuration file for custom forms_test module.
 */

class FormsTestConfig {
  //menu paths
  const TEST_PAGE_PATH = 'test/forms';
  //form ID
  const SIMPLE_TEST_FORM_ID = 'forms_test_simple_form';
}
