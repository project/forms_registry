<?php
/**
 * @file
 * A sample config file for the simple form test.
 */


/**
 * Returns a list of countries to be used by the simple form.
 */
function forms_test_get_countries() {
  return array(
    1 => 'Namibia',
    2 => 'Macau',
    3 => 'Turkmenistan',
    4 => 'Other...',
  );
}